// select a database
use <database name>

// when creating a new database via the command line, the use command can be entered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created.

// database = filing cabinet
// collection = drawer
// document/record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file content

/*
e.g document with no sub-documents:

	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 Street St",
			city: "Makati",
			country: "Philippines"
		}

	}
*/


// Embeded vs Referred


// Example of Referred

/* Users:

	{
		id: 300,
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}

	Orders:
		{
			products: [
				{
					name: "New Pillow",
					price: 300
				}
			]
			userId: 300
		}

*/

// Embedded Data

/*

{
	id: 298,
	name: "Jino Yumul",
	age: 33,
	occupation: "Instructor",
	orders: [
		{
			products: "New PIllow",
			price: 300
		}
	]
}

*/

// Insert One Document (Create)

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})


// if inserting/creating a new document within a collection that does not yet exist, MOngoDB will automatically create a collection

// insert many

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "123456789",
				email: "stephenhawking@mail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "123456789",
				email: "neilarmstrong@mail.com"
			},
			courses: ["React", "Laravel", "SASS"],
			department: "none"
		}
	])



db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "123456789",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
})

// Finding documents (Read)

// Retrieves a list of ALL Users
db.users.find()

// Find a Specific User
db.users.find({firstName: "Stephen"}) /*finds any and all matches*/
db.users.findOne({firstName: "Stephen"}) /*finds only the first match*/

// Find documents with multiple paramentes/conditions
db.users.find({lastName: "Armstrong", age: 82})

// Update/Edit Documents

// Create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "123456789",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
})

db.users.updateOne(
	{_id: ObjectId("62876e3c9c1ddf434e7fb17d")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	}
)

// update multiple documents
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
	)

// creating a document to delete
db.users.insert({
	firstName: "test"
})

// deleting a single document
db.users.deleteOne({
	firstName: "test"
})

// deleting many records
// Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection

// create another user to delete
db.users.insert({
	firstName: "Bill"
})

db.users.deleteMany({
	firstName: "Bill"
})
